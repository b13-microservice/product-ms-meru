FROM openjdk:8
EXPOSE 8084
ARG JAR_FILE=target/product-service-meru.jar
WORKDIR /opt/app
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]