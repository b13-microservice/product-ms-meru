package com.capstone.productmsmeru.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.capstone.productmsmeru.model.*;
import com.capstone.productmsmeru.service.ProductService;

import java.util.*;

@RestController
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@PostMapping("/saveorder")
	public OrderResponse saveOrder(@RequestBody FormData formData) {
		
		return productService.saveOrderDetails(formData);
	}
	
	@GetMapping("/productsByCategory/{category}")
	public List<Object> getDetailsByProdCategory(@PathVariable String category) {
		
		return productService.getProductDeatilsByCategory(category);
	}
	
	
	@GetMapping("/productsByName/{name}")
	public List<Object> getDetailsByProdName(@PathVariable String name) {
		
		return productService.getProductDeatilsByName(name);
	}
	
	
	
	

}
