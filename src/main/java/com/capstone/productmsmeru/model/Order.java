package com.capstone.productmsmeru.model;

import java.util.*;
public class Order {
	
private Integer id;
	
	private String name;
	private int qty;
    private double price;
    
    List<Product> prodList = new ArrayList<Product>();
	public Order() {
		super();
	}
	public Order(Integer id, String name, int qty, double price, List<Product> prodList) {
		super();
		this.id = id;
		this.name = name;
		this.qty = qty;
		this.price = price;
		this.prodList = prodList;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public List<Product> getProdList() {
		return prodList;
	}
	public void setProdList(List<Product> prodList) {
		this.prodList = prodList;
	}
	@Override
	public String toString() {
		return "Order [id=" + id + ", name=" + name + ", qty=" + qty + ", price=" + price + ", prodList=" + prodList
				+ "]";
	}
	
	
    
	
	
	

}
