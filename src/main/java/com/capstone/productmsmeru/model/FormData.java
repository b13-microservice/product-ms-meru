package com.capstone.productmsmeru.model;

public class FormData {

	private Order order;
	
	

	public FormData() {
		super();
	}



	public FormData(Order order) {
		super();
		this.order = order;
	}



	public Order getOrder() {
		return order;
	}



	public void setOrder(Order order) {
		this.order = order;
	}



	@Override
	public String toString() {
		return "FormData [order=" + order + "]";
	}
	
	
	
}
