package com.capstone.productmsmeru.model;
import java.util.*;
public class OrderResponse {

	private Payment payment;
	
	private List<Product> prodList;
	
	private String responseMessage;
	

	
	
	public OrderResponse() {
		super();
	}






	public OrderResponse(Payment payment, List<Product> prodList, String responseMessage) {
		super();
		this.payment = payment;
		this.prodList = prodList;
		this.responseMessage = responseMessage;
	}






	public OrderResponse(String responseMessage) {
		super();
		this.responseMessage = responseMessage;
	}






	public Payment getPayment() {
		return payment;
	}






	public void setPayment(Payment payment) {
		this.payment = payment;
	}






	public List<Product> getProdList() {
		return prodList;
	}






	public void setProdList(List<Product> prodList) {
		this.prodList = prodList;
	}






	public String getResponseMessage() {
		return responseMessage;
	}






	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}






	@Override
	public String toString() {
		return "OrderResponse [payment=" + payment + ", prodList=" + prodList + ", responseMessage=" + responseMessage
				+ "]";
	}



	
	
}