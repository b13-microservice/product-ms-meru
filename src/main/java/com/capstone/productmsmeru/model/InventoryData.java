package com.capstone.productmsmeru.model;

import java.util.*;
public class InventoryData {
	
	private Map<Integer,Integer> inventoryMap = new HashMap<Integer,Integer>();
	
	

	public InventoryData() {
		super();
	}

	public InventoryData(Map<Integer, Integer> inventoryMap) {
		super();
		this.inventoryMap = inventoryMap;
	}

	public Map<Integer, Integer> getInventoryMap() {
		return inventoryMap;
	}

	public void setInventoryMap(Map<Integer, Integer> inventoryMap) {
		this.inventoryMap = inventoryMap;
	}

	@Override
	public String toString() {
		return "InventoryData [inventoryMap=" + inventoryMap + "]";
	}
	
	
	
	

}
