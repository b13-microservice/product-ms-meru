package com.capstone.productmsmeru.model;

public class Product {

	private Integer inventoryId;
	private String name;
	private int qty;
    private double price;
    

    
    public Product() {
		super();
	}



	public Product(Integer inventoryId, String name, int qty, double price) {
		super();
		this.inventoryId = inventoryId;
		this.name = name;
		this.qty = qty;
		this.price = price;
	}



	public Integer getInventoryId() {
		return inventoryId;
	}



	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public int getQty() {
		return qty;
	}



	public void setQty(int qty) {
		this.qty = qty;
	}



	public double getPrice() {
		return price;
	}



	public void setPrice(double price) {
		this.price = price;
	}


	


	
    
    
}
