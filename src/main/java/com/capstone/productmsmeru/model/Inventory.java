package com.capstone.productmsmeru.model;

public class Inventory {
	
private Integer id;
	
	private String productName;
	
	private String productCategory;
	
	private float price;
	
	private int quantity;
	
	private String vendorName;

	public Inventory() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Inventory(Integer id, String productName, String productCategory, float price, int quantity,
			String vendorName) {
		super();
		this.id = id;
		this.productName = productName;
		this.productCategory = productCategory;
		this.price = price;
		this.quantity = quantity;
		this.vendorName = vendorName;
	}
	
	public Inventory(String productName, String productCategory, float price, int quantity,
			String vendorName) {
		super();
		this.productName = productName;
		this.productCategory = productCategory;
		this.price = price;
		this.quantity = quantity;
		this.vendorName = vendorName;
	}
	

	/**
	 * @return the id
	 */
	public Integer getid() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setid(Integer id) {
		this.id = id;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the productCategory
	 */
	public String getProductCategory() {
		return productCategory;
	}

	/**
	 * @param productCategory the productCategory to set
	 */
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	@Override
	public String toString() {
		return "Inventory[id=" +this.id + " ,productName=" + this.productName +
				", productCategory = " + this.productCategory + ", price = " + this.price + 
				", quantity = " + this.quantity + ", vendorName=" + this.vendorName + "]";
	}

}
