package com.capstone.productmsmeru.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.capstone.productmsmeru.model.FormData;
import com.capstone.productmsmeru.model.InventoryData;
import com.capstone.productmsmeru.model.Order;
import com.capstone.productmsmeru.model.OrderResponse;
import com.capstone.productmsmeru.model.Product;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class ProductService {
	
	
	@Autowired
	RestTemplate restTemplate;
	

	@HystrixCommand(fallbackMethod = "saveOrderFallback",
			commandProperties = {
					@HystrixProperty(
							name = "circuitBreaker.requestVolumeThreshold",value="4"),
					@HystrixProperty(
							name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000")
			})
	public OrderResponse saveOrderDetails(FormData formData) {
		
		HttpHeaders headers = new HttpHeaders();
		
		System.out.println("Form Data: "+formData);
		
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		HttpEntity<FormData> entity = new  HttpEntity<FormData>(formData,headers);
		String url = "http://ORDER-SERVICE-MERU/order/saveOrder";
		
		OrderResponse orderResponse = restTemplate.exchange(url, HttpMethod.POST,entity, OrderResponse.class).getBody();
		
		//testing to handle orderResponse correctly 
		
		if(orderResponse.getPayment() == null)
		{
			return new OrderResponse("Payment not done But Order is saved in Cart ");
			
		}
		
		
		//end
		
		//update inventory
		
		Map<Integer,Integer> inventoryMap = new HashMap<Integer,Integer>();
		
		List<Product> prodList = formData.getOrder().getProdList();
		
		prodList.forEach(prod -> {
			
			inventoryMap.put(prod.getInventoryId(),prod.getQty());
		});
		
		String msgInventory = updateInventory(inventoryMap);
			
		//end
		
		return orderResponse;
	}

	@HystrixCommand(fallbackMethod = "updateInvFallback",
			commandProperties = {
					@HystrixProperty(
							name = "circuitBreaker.requestVolumeThreshold",value="4"),
					@HystrixProperty(
							name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000")
			})
	private String updateInventory(Map<Integer, Integer> inventoryMap) {
		
		String url = "http://INVENTORY-SERVICE-MERU/inventory/product";
		InventoryData inventoryData = new InventoryData();
		
		HttpHeaders headers = new HttpHeaders();
		
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		inventoryData.setInventoryMap(inventoryMap);
		
		HttpEntity<InventoryData> entity = new  HttpEntity<InventoryData>(inventoryData,headers);
		
		String msg = restTemplate.exchange(url, HttpMethod.PUT,entity,String.class).getBody();
		
		return msg;
	}

	@HystrixCommand(fallbackMethod = "getProductDeatilsByNameFallback",
			commandProperties = {
					@HystrixProperty(
							name = "circuitBreaker.requestVolumeThreshold",value="4"),
					@HystrixProperty(
							name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000")
			})
	public List<Object> getProductDeatilsByCategory(String category) {
		
		System.out.println("category: "+category);

		String url = "http://INVENTORY-SERVICE-MERU/inventory/productsByCategory/"+category;
		
		System.out.println("url:: "+url);
		
		Object[] objects = restTemplate.getForObject(url,Object[].class);
		
		return Arrays.asList(objects);
	}


	
	

	@HystrixCommand(fallbackMethod = "getProductDeatilsByNameFallback",
			commandProperties = {
					@HystrixProperty(
							name = "circuitBreaker.requestVolumeThreshold",value="4"),
					@HystrixProperty(
							name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000")
			})
	public List<Object> getProductDeatilsByName(String name) {

		String url = "http://INVENTORY-SERVICE-MERU/inventory/productsByName/"+name;
		
		System.out.println("url:: "+url);
		
		Object[] objects = restTemplate.getForObject(url,Object[].class);
		
		return Arrays.asList(objects);
	}
	
	
	@SuppressWarnings("unused2")
	public List<Object>  getProductDeatilsByNameFallback(String name) {
		
		String s= "Unable to fetch products";
		Object object=s;
		return  Arrays.asList(object);
	}
	
	
	@SuppressWarnings("unused1")
	public OrderResponse saveOrderFallback(FormData formData) {
		
		
		return new OrderResponse("We Are Unable place your  Order , Try after some time  ");
	}
	
	public String updateInvFallback(Map<Integer, Integer> inventoryMap) {
		
		return "Sorry We Are Unable to take Your Order"; 
	}
	
	
	
}
